// Author: Thorbjoern
// Find all primes less than or equal to the input. 
// Display the largest of them.

#include<iostream>
#include<vector>

std::vector<int> get_all_numbers(int max_value)
{
    std::vector<int> range_values;
    for(int i=0;i<max_value;i++)
    {
        range_values.push_back(i+1);
    }
    return range_values;
}

// Find all primes using the Sieve of Eratosthenes
std::vector<int> all_primes(std::vector<int> range_values)
{
    for(int i=2;i<(range_values.size()+1);i++)
    {
        for(int j=i*i; j<(range_values.size()+1);j+=i)
        {

            range_values[j-1] = 0;
        }
    }
    std::vector<int> primes;
    for(int i=0;i<range_values.size(); i++)
    {
        if(range_values[i]>0)
        {
            primes.push_back(range_values[i]);
        }
    }
    return primes;
}

int main()
{
    int number;
    std::cout<<"Input a number: "<<std::endl;
    std::cin>>number;

    std::vector<int> all_num = get_all_numbers(number);
    std::vector<int> primes = all_primes(all_num);

    int largest_prime = primes[primes.size()-1]; 

    std::cout<<"Largest prime less than "<<number<<" is "<<largest_prime<<std::endl;

    return 0;
}