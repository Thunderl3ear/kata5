# Kata 5 - Finding primes
Takes a user input integer and finds all primes less than or equal to that number. Displays the largest of those.

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [License](#license)

## Requirements
* Requires `gcc`.

## Usage
* mkdir build
* Compile with: g++ main.cpp -o build/main
* Run: ./build/main

## Maintainers

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

## License

---
Copyright 2022, Thorbjørn Koch ([@Thunderl3ear](https://gitlab.com/Thunderl3ear))
